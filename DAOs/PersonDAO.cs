using System.Data.SqlClient;
using CondomeeAPI.Models;
using Dapper;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace Condomee.DAOs
{
  public class PersonDAO
  {
    private IConfiguration _configuration;

    public PersonDAO(IConfiguration configuration)
    {
      _configuration = configuration;
    }

    public Person Find(string personEmail)
    {
      using (MySqlConnection conexao = new MySqlConnection(
        _configuration.GetConnectionString("DefaultConnection")))
      {
        return conexao.QueryFirstOrDefault<Person>(
          "SELECT * FROM condomee.Person WHERE email = @Email", new { Email = personEmail });
      }
    }
  }
}