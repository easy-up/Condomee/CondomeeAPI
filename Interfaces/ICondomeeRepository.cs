using System.Collections.Generic;

namespace CondomeeAPI.Interfaces
{
    public interface ICondomeeRepository<T>
    {
        bool Exist(long id);
        IEnumerable<T> FindAll { get; }
        T FindByID(long id);
        void Insert(T element);
        void Update(T element);
        void DeleteByID(long id);
    }
}