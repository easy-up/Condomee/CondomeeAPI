﻿using System;
using System.Collections.Generic;

namespace CondomeeAPI.Models
{
    public partial class CondominiumHasPerson
    {
        public int CondominiumIdCondominium { get; set; }
        public int PersonIdPerson { get; set; }
        public int PerfilIdPerfil { get; set; }

        public virtual Condominium CondominiumIdCondominiumNavigation { get; set; }
        public virtual Perfil PerfilIdPerfilNavigation { get; set; }
        public virtual Person PersonIdPersonNavigation { get; set; }
    }
}
