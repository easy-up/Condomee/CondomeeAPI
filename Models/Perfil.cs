﻿using System;
using System.Collections.Generic;

namespace CondomeeAPI.Models
{
    public partial class Perfil
    {
        public Perfil()
        {
            CondominiumHasPerson = new HashSet<CondominiumHasPerson>();
        }

        public int IdPerfil { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CondominiumHasPerson> CondominiumHasPerson { get; set; }
    }
}
