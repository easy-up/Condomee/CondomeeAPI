﻿using System;
using System.Collections.Generic;

namespace CondomeeAPI.Models
{
    public partial class Condominium
    {
        public Condominium()
        {
            CondominiumHasPerson = new HashSet<CondominiumHasPerson>();
        }

        public int IdCondominium { get; set; }
        public string Name { get; set; }

        public ICollection<CondominiumHasPerson> CondominiumHasPerson { get; set; }
    }
}
