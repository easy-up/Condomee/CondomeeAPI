﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CondomeeAPI.Models
{
    public partial class condomeeContext : DbContext
    {
        public condomeeContext()
        {

        }

        public condomeeContext(DbContextOptions<condomeeContext> options)
            : base(options)
        {

        }

        public virtual DbSet<Condominium> Condominium { get; set; }
        public virtual DbSet<CondominiumHasPerson> CondominiumHasPerson { get; set; }
        public virtual DbSet<Perfil> Perfil { get; set; }
        public virtual DbSet<Person> Person { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("Server=localhost;Database=condomee;User=root;Password=ffhfkkw15;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Condominium>(entity =>
            {
                entity.HasKey(e => e.IdCondominium);

                entity.Property(e => e.IdCondominium)
                    .HasColumnName("idCondominium")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(45)");
            });

            modelBuilder.Entity<CondominiumHasPerson>(entity =>
            {
                entity.HasKey(e => new { e.CondominiumIdCondominium, e.PersonIdPerson, e.PerfilIdPerfil });

                entity.ToTable("Condominium_has_Person");

                entity.HasIndex(e => e.CondominiumIdCondominium)
                    .HasName("fk_Condominium_has_Person_Condominium_idx");

                entity.HasIndex(e => e.PerfilIdPerfil)
                    .HasName("fk_Condominium_has_Person_Perfil1_idx");

                entity.HasIndex(e => e.PersonIdPerson)
                    .HasName("fk_Condominium_has_Person_Person1_idx");

                entity.Property(e => e.CondominiumIdCondominium)
                    .HasColumnName("Condominium_idCondominium")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PersonIdPerson)
                    .HasColumnName("Person_idPerson")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PerfilIdPerfil)
                    .HasColumnName("Perfil_idPerfil")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.CondominiumIdCondominiumNavigation)
                    .WithMany(p => p.CondominiumHasPerson)
                    .HasForeignKey(d => d.CondominiumIdCondominium)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Condominium_has_Person_Condominium");

                entity.HasOne(d => d.PerfilIdPerfilNavigation)
                    .WithMany(p => p.CondominiumHasPerson)
                    .HasForeignKey(d => d.PerfilIdPerfil)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Condominium_has_Person_Perfil1");

                entity.HasOne(d => d.PersonIdPersonNavigation)
                    .WithMany(p => p.CondominiumHasPerson)
                    .HasForeignKey(d => d.PersonIdPerson)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Condominium_has_Person_Person1");
            });

            modelBuilder.Entity<Perfil>(entity =>
            {
                entity.HasKey(e => e.IdPerfil);

                entity.Property(e => e.IdPerfil)
                    .HasColumnName("idPerfil")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("varchar(45)");
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.HasKey(e => e.IdPerson);

                entity.Property(e => e.IdPerson)
                    .HasColumnName("idPerson")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnType("varchar(45)");
            });
        }
    }
}
