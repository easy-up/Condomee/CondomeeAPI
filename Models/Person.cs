﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace CondomeeAPI.Models
{
    public partial class Person
    {
        public Person()
        {
            CondominiumHasPerson = new HashSet<CondominiumHasPerson>();
        }

        [Key]
        [Required]
        public int IdPerson { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }

        [JsonProperty("Condominiuns")]
        public virtual ICollection<CondominiumHasPerson> CondominiumHasPerson { get; set; }
    }
}
