using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CondomeeAPI.Models;

namespace CondomeeAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CondominiumHasPersonController : ControllerBase
    {
        private readonly condomeeContext _context;

        public CondominiumHasPersonController(condomeeContext context)
        {
            _context = context;
        }

        // GET: api/CondominiumHasPerson
        [HttpGet]
        public IEnumerable<CondominiumHasPerson> GetCondominiumHasPerson()
        {
            return _context.CondominiumHasPerson;
        }

        // GET: api/CondominiumHasPerson/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCondominiumHasPerson([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var condominiumHasPerson = await _context.CondominiumHasPerson.FindAsync(id);

            if (condominiumHasPerson == null)
            {
                return NotFound();
            }

            return Ok(condominiumHasPerson);
        }

        // PUT: api/CondominiumHasPerson/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCondominiumHasPerson([FromRoute] int id, [FromBody] CondominiumHasPerson condominiumHasPerson)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != condominiumHasPerson.CondominiumIdCondominium)
            {
                return BadRequest();
            }

            _context.Entry(condominiumHasPerson).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CondominiumHasPersonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CondominiumHasPerson
        [HttpPost]
        public async Task<IActionResult> PostCondominiumHasPerson([FromBody] CondominiumHasPerson condominiumHasPerson)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.CondominiumHasPerson.Add(condominiumHasPerson);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CondominiumHasPersonExists(condominiumHasPerson.CondominiumIdCondominium))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCondominiumHasPerson", new { id = condominiumHasPerson.CondominiumIdCondominium }, condominiumHasPerson);
        }

        // DELETE: api/CondominiumHasPerson/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCondominiumHasPerson([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var condominiumHasPerson = await _context.CondominiumHasPerson.FindAsync(id);
            if (condominiumHasPerson == null)
            {
                return NotFound();
            }

            _context.CondominiumHasPerson.Remove(condominiumHasPerson);
            await _context.SaveChangesAsync();

            return Ok(condominiumHasPerson);
        }

        private bool CondominiumHasPersonExists(int id)
        {
            return _context.CondominiumHasPerson.Any(e => e.CondominiumIdCondominium == id);
        }
    }
}