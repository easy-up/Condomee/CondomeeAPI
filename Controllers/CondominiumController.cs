using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CondomeeAPI.Models;
using Microsoft.AspNetCore.Authorization;

namespace CondomeeAPI.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class CondominiumController : ControllerBase
  {
    private readonly condomeeContext _context;

    public CondominiumController(condomeeContext context)
    {
      _context = context;
    }

    // GET: api/Condominium
    [Authorize("Bearer")]
    [HttpGet]
    public IEnumerable<Condominium> GetCondominium()
    {
      return _context.Condominium;
    }

    // GET: api/Condominium/5
    [HttpGet("{id}")]
    public async Task<IActionResult> GetCondominium([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var condominium = await _context.Condominium.FindAsync(id);

      if (condominium == null)
      {
        return NotFound();
      }

      return Ok(condominium);
    }

    // PUT: api/Condominium/5
    [HttpPut("{id}")]
    public async Task<IActionResult> PutCondominium([FromRoute] int id, [FromBody] Condominium condominium)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != condominium.IdCondominium)
      {
        return BadRequest();
      }

      _context.Entry(condominium).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!CondominiumExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/Condominium
    [HttpPost]
    public async Task<IActionResult> PostCondominium([FromBody] Condominium condominium)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      _context.Condominium.Add(condominium);
      await _context.SaveChangesAsync();

      return CreatedAtAction("GetCondominium", new { id = condominium.IdCondominium }, condominium);
    }

    // DELETE: api/Condominium/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteCondominium([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var condominium = await _context.Condominium.FindAsync(id);
      if (condominium == null)
      {
        return NotFound();
      }

      _context.Condominium.Remove(condominium);
      await _context.SaveChangesAsync();

      return Ok(condominium);
    }

    private bool CondominiumExists(int id)
    {
      return _context.Condominium.Any(e => e.IdCondominium == id);
    }
  }
}