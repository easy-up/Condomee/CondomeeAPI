using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using Condomee.DAOs;
using CondomeeAPI.Controllers;
using CondomeeAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace Condomee.Controllers
{
  [Route("api/[controller]")]
  public class LoginController : Controller
  {
    [AllowAnonymous]
    public Object Post(
      [FromBody]Person person,
      [FromServices]PersonDAO personDao,
      [FromServices]SigningConfigurations signingConfigurations,
      [FromServices]TokenConfigurations tokenConfigurations)
    {
      bool credenciaisValidas = false;
      if (person != null)
      {
        var personBase = personDao.Find(person.Email);
        credenciaisValidas = (personBase != null &&
        person.Email == personBase.Email &&
        person.Password == personBase.Password);
      }

      if (credenciaisValidas)
      {
        ClaimsIdentity identity = new ClaimsIdentity(
          new GenericIdentity(person.Email, "Login"),
          new[]{
              new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
              new Claim(JwtRegisteredClaimNames.Email, person.Email),
          }
        );

        DateTime dataCriacao = DateTime.Now;
        DateTime dataExpiracao = dataCriacao +
          TimeSpan.FromSeconds(tokenConfigurations.Seconds);

        var handler = new JwtSecurityTokenHandler();
        var securityToken = handler.CreateToken(new SecurityTokenDescriptor
        {
          Issuer = tokenConfigurations.Issuer,
          Audience = tokenConfigurations.Audience,
          SigningCredentials = signingConfigurations.SigningCredentials,
          Subject = identity,
          NotBefore = dataCriacao,
          Expires = dataExpiracao
        });
        var token = handler.WriteToken(securityToken);

        return new
        {
          authenticated = true,
          created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
          expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
          accessToken = token,
          message = "OK"
        };
      }
      else
      {
        return new
        {
          authenticated = false,
          message = "Falha ao autenticar"
        };
      }
    }

    [HttpGet("/verify")]
    [Authorize("Bearer")]
    public Boolean VerifyToken()
    {
      return true;
    }
  }
}