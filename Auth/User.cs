namespace Condomee
{
  public class User
  {
    public string UserID { get; set; }
    public string AccessKey { get; set; }
  }

  public class TokenConfigurations
  {
    public string Audience { get; set; }
    public string Issuer { get; set; }
    public int Seconds { get; set; }
  }
}